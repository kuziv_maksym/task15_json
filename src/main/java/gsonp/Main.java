package gsonp;

import com.epam.jsonp.Employee;
import com.google.gson.Gson;

public class Main {
    public static void main(String[] args) {
        Gson gson = new Gson();
        Employee employee = new Employee();
        employee.setName("Maks");
        employee.setAge(20);
        employee.setWork(false);

        String result = gson.toJson(employee);
        System.out.println(result);

        Employee result2 = gson.fromJson(result, Employee.class);
        System.out.println(result2);
    }
}
