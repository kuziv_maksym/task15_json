package com.epam.jsonp;

public class Employee {
    private String name;
    private int age;
    private boolean work;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
    public boolean getWork() {
        return work;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return getName() + " " + getAge() + " " + getWork();
    }
}
