package com.epam.jsonp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Employee employee = new Employee();
        employee.setName("Maks Kuziv");
        employee.setAge(20);
        employee.setWork(false);
        String result = objectMapper.writeValueAsString(employee);
        System.out.println(result);

        Employee newEmployee = objectMapper.readValue(result, Employee.class);

       System.out.println(newEmployee);

    }
}
